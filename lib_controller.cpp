//
// Created by lx8r on 14.11.2016.
//
#include "lib_controller.h"

int Controller::read_key(){
    int key_val=-1;
    while(key_val<0){
        std::cin >> key_val;
    }

    return key_val;
}


bool Controller::main_loop(Model &model, View &view) {
    int key=1;
    while( key != 0) {
        view.show_menu();
        key=read_key();

        switch(key) {
            case 1:
                user_loop(model,view);
                break;
            case 2:
                book_loop(model,view);
                break;
            case 3:
                overdue_book_loop(model,view);
                break;
            case 4:
                day_loop(model, view);
                break;
        }
    }
}

bool Controller::user_loop(Model &model, View &view){
    view.show_users(model.users);
    int key = read_key();

    if(key < 0)
        return false;
    else if(key > 0 ){
        int user_id = key;
        view.add_menu();
        key = read_key();

        view.show_library(model.books);

        if(key == 1){                                       //Adding book
            int book_id = read_key();
            model.give_book_to_user(user_id, book_id);
        }else if(key == 2){                                 //Receiving book
            int book_id = read_key();
            model.take_book_from_user(user_id, book_id);
        }

    }

    return true;
}

bool Controller::book_loop(Model &model, View &view){
    view.show_library(model.books);
    int key = read_key();

    if(key < 0)
        return false;
    else if(key > 0 ){
        int book_id = key;
        view.add_menu();
        key = read_key();

        view.show_users(model.users);

        if(key == 1){                                       //Adding book
            int user_id = read_key();
            model.give_book_to_user(user_id, book_id);
        }else if(key == 2){                                 //Receiving book
            int user_id = read_key();
            model.take_book_from_user(user_id, book_id);
        }

    }

    return true;
}

bool Controller::overdue_book_loop(Model &model, View &view){
    view.show_library(model.get_overdue_books());
    int key = read_key();

    if(key < 0)
        return false;
    else if(key > 0 ){
        int book_id = key;
        view.add_menu();
        key = read_key();

        view.show_users(model.users);

        if(key == 1){                                       //Adding book
            int user_id = read_key();
            model.give_book_to_user(user_id, book_id);
        }else if(key == 2){                                 //Receiving book
            int user_id = read_key();
            model.take_book_from_user(user_id, book_id);
        }

    }

    return true;
}

bool Controller::day_loop(Model &model, View &view){
    view.show_day(model.get_day());
    int key = read_key();

    if(key>0){
        model.next_day(key);
    }

    return true;
}