//
// Created by lx8r on 10.11.2016.
//

#ifndef SB_LIB_LIB_VIEW_H
#define SB_LIB_LIB_VIEW_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>
#include "lib_model.h"


class View{
private:

public:
    bool show_menu();
    bool add_menu();
    bool show_users(std::vector<struct User> str_vec);
    bool show_library(std::vector<struct Book> str_vec);
    void show_day(int);
};
#endif //SB_LIB_LIB_VIEW_H
