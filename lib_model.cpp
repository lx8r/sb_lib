//
// Created by lx8r on 14.11.2016.
//

#include "lib_model.h"

void Model::add_book(std::string title) {
    struct Book new_book;

    new_book.id=++(last_book_id);
    new_book.title = title;

    books.push_back(new_book);
}

void Model::add_user(std::string name) {
    struct User new_user;

    new_user.id=++(last_user_id);
    new_user.name = name;

    users.push_back(new_user);
}

bool Model::give_book_to_user(int user_id, int book_id) {
    if( (user_id > last_user_id) ||
        (book_id > last_book_id) ||
        (book_id <= 0) ||
        (user_id <= 0)){

        std::cout << "Illegal user id or book id\n";
        return false;
    }

    std::vector<struct User>::iterator it_user = users.begin();
    std::vector<struct Book>::iterator it_book = books.begin();



    for(it_user ; it_user != users.end(); ++it_user ){               //Find user
        if(it_user->id == user_id)
            break;
    }

    if(it_user == users.end()){                                      //User exists
        std::cout << "Unknown user!\n";
        return false;
    }

    for(it_book ; it_book != books.end(); ++it_book ){               //Find book
        if(it_book->id == book_id)
            break;
    }

    if(it_book == books.end()){                                      //Book exists
        std::cout << "Unknown book!\n";
        return false;
    }

    if(it_book->user_id != 0){                                             //Free book
        std::cout << "The user " << users[it_book->user_id-1].name << " get this book!\n";
        return false;
    }


    int i;                                                                 //Adding book to user
    for(i=0; i<3; i++){
        if( it_user->book_id[i] == 0 ){
            it_user->book_id[i] = it_book->id;
            it_book->user_id = it_user->id;
            it_book->day = present_day;
            break;
        }
    }
    if(i == 3){
        std::cout<< "The user "<< it_user->name << " has 3 books!\n";
        return false;
    }

    return true;
}

bool Model::take_book_from_user(int user_id, int book_id){
    if( (user_id > last_user_id) ||
        (book_id > last_book_id) ||
        (book_id <= 0) ||
        (user_id <= 0)){

        std::cout << "Illegal user id or book id\n";
        return false;
    }

    std::vector<struct User>::iterator it_user = users.begin();
    std::vector<struct Book>::iterator it_book = books.begin();



    for(it_user ; it_user != users.end(); ++it_user ){               //Find user
        if(it_user->id == user_id)
            break;
    }

    if(it_user == users.end()){                                      //User exists
        std::cout << "Unknown user!\n";
        return false;
    }

    for(it_book ; it_book != books.end(); ++it_book ){               //Find book
        if(it_book->id == book_id)
            break;
    }

    if(it_book == books.end()){                                      //Book exists
        std::cout << "Unknown book!\n";
        return false;
    }



    for(int i=0; i<3; i++){                                                    //Receiving book from user
        if( it_user->book_id[i] == book_id ){
            it_user->book_id[i] = 0;
            it_book->user_id = 0;
            it_book->day = 0;
            break;
        }
    }

    return true;
}

int Model::get_day() {
    return present_day;
}

bool Model::next_day(int days) {
    if(days<0){
        std::cout << "Days must be greater then 0!\n";
        return false;
    }

    if(present_day+days>1000000){
        std::cout << "Too many days!\n";
        return false;
    }

    present_day += days;
    return true;
}

std::vector<struct Book> Model::get_overdue_books() {
    std::vector<struct Book> overdue_books;

    std::vector<struct Book>::iterator it_book = books.begin();

    for(it_book ; it_book != books.end(); ++it_book ){               //Adding overdue book
        if( (present_day-it_book->day) > 30 ){
            if(it_book->day != 0){
                overdue_books.push_back(*it_book);
            }
        }
    }

    return  overdue_books;
}