//
// Created by lx8r on 10.11.2016.
//
#include"lib_view.h"


bool View::show_menu() {
    system("cls");
    std::cout << "1. Show Users\n";
    std::cout << "2. Show Library\n";
    std::cout << "3. Show Overdue Books\n";
    std::cout << "4. Edit Day\n";
    std::cout << "0. Exit\n";

    return true;
}

bool View::add_menu() {
    system("cls");
    std::cout << "1. Add book to the user\n";
    std::cout << "2. Get book from the user\n";
    std::cout << "0. Exit\n";

    return true;
}

bool View::show_users(std::vector<struct User> str_vec) {
    system("cls");
    if( str_vec.empty() ) {
        std::cout << "Can't show users!\n";
        return false;
    }

    std::vector<struct User>::iterator it = str_vec.begin();

    for (it ; it != str_vec.end(); ++it){
        std::cout << it->id <<"\t";
        std::cout << it->name <<"\t";
        std::cout << "[ " << it->book_id[0] << ", " << it->book_id[1]<< ", " << it->book_id[2] <<"]\n";
    }

    std::cout << "To choose the user, enter his number.\n";
    std::cout << "To exit, enter 0\n";

    return true;
}

bool View::show_library(std::vector<struct Book> str_vec) {
    system("cls");
    if(str_vec.empty()){
        std::cout << "Can't show library!\n";
        return false;
    }

    std::vector<struct Book>::iterator it = str_vec.begin();

    for (it ; it != str_vec.end(); ++it){
        std::cout << it->id <<"\t";
        std::cout << it->title <<"\t";
        std::cout << "User: " << it->user_id << " took this book at: " << it->day<<" day\n";
    }

    std::cout << "To choose a book, enter its number.\n";
    std::cout << "To exit, enter 0\n";


    return true;
}

void View::show_day(int days){
    system("cls");
    std::cout << "Now is " << days << " day.\n";
    std::cout << "To add days, enter count of days\n";
    std::cout << "To exit, enter 0\n";
}

