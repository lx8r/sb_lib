//
// Created by lx8r on 14.11.2016.
//

#ifndef SB_LIB_LIB_CONTROLLER_H
#define SB_LIB_LIB_CONTROLLER_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>

#include"lib_view.h"

class Controller{
private:
    int read_key();
public:

    bool main_loop( Model &model,  View &view);
    bool user_loop( Model &model,  View &view);
    bool book_loop( Model &model,  View &view);
    bool overdue_book_loop( Model &model, View &view);
    bool day_loop( Model &model,  View &view);
};

#endif //SB_LIB_LIB_CONTROLLER_H
