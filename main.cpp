#include "lib_view.h"
#include "lib_model.h"
#include "lib_controller.h"

void filling(Model &lib_model);

int main() {

    View lib_view;
    Model lib_model;
    Controller lib_control;


    filling(lib_model);
    lib_control.main_loop(lib_model, lib_view);

    return 0;
}

void filling(Model &lib_model){
    lib_model.add_user("Root");
    lib_model.add_user("User_1");
    lib_model.add_user("User_2");
    lib_model.add_user("User_3");
    lib_model.add_user("User_4");

    lib_model.add_book("Book_1");
    lib_model.add_book("Book_2");
    lib_model.add_book("Book_3");
    lib_model.add_book("Book_4");
    lib_model.add_book("Book_5");
    lib_model.add_book("Book_6");
    lib_model.add_book("Book_7");
    lib_model.add_book("Book_8");
    lib_model.add_book("Book_9");
    lib_model.add_book("Book_10");
    lib_model.add_book("Book_11");
    lib_model.add_book("Book_12");
    lib_model.add_book("Book_13");
}

/*
    lib_model.give_book_to_user(1,1);
    lib_model.give_book_to_user(2,2);
    lib_model.next_day(10);
    lib_model.give_book_to_user(2,3);
    lib_model.take_book_from_user(1,1);
    lib_model.next_day(21);
    lib_model.give_book_to_user(2,1);
    lib_model.give_book_to_user(2,4);
    lib_view.show_library(lib_model.books);
    lib_view.show_users(lib_model.users);
    lib_view.show_library(lib_model.get_overdue_books());
 */