//
// Created by lx8r on 14.11.2016.
//

#ifndef SB_LIB_LIB_MODEL_H
#define SB_LIB_LIB_MODEL_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>

struct Book{
    unsigned id=0;
    std::string title="";
    unsigned user_id=0;
    unsigned day=0;
};

struct User{
    unsigned id=0;
    std::string name="";
    unsigned book_id[3]={0,0,0};
};


class Model{
private:
    unsigned present_day;
    unsigned last_book_id;
    unsigned last_user_id;

public:
    std::vector<struct Book> books;
    std::vector<struct User> users;

    Model():present_day(1), last_book_id(0), last_user_id(0){}

    void add_book(std::string title);
    void add_user(std::string name);

    bool give_book_to_user(int user_id, int book_id);
    bool take_book_from_user(int user_id, int book_id);

    int get_day();
    bool next_day(int days);

    std::vector<struct Book> get_overdue_books();
};
#endif //SB_LIB_LIB_MODEL_H
